### reconstruction based on ICA basis
#### 89 subjects, reconstructed via dual regression ICA maps * unseen reconstruction coefficients
#### blue curve (Steve's HCP dual regression maps); orange curve (Steve's ICA on MIGP + my dual regression scripts) green curve (fastica + my dual regression maps)
#### orange curve and green curve: dual regression was run twice; time courses normalised across time; spatial maps were taken as zstat
#### ICA components were sorted by variance explained, but still there is a mismatch between fastica output and the HCP groupICA components
#### Instead of runing ICA once (d=100), should we run ICA 100 times (each time d=k, k=1,2,3...100) to plot the curve?
![](figs/curves_ica.png)

### reconstructed maps vs actual maps (89 by 89 subjects)
#### fastica basis, normalised across row and column, at d=100
![](figs/corr_mat_z.png)

#### fastica basis, unnormalised, at d=100
![](figs/corr_mat.png)

#### fastica basis, unnormalised, at d=100, with global task activation regressed out
![](figs/corr_mat_reg.png)

#### fastica basis, unnormalised, at d=100, with group ICA regressed out from individual dual regression maps
![](figs/corr_mat_reg2.png)

#### fastica basis, unnormalised, at d=100, with global task activation regressed out and group ICA regressed out
![](figs/corr_mat_reg3.png)

