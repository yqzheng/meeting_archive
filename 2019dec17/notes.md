## Visualisation of the first three laplacian eigenmaps (group-level and two subjects, 100206 and 100307)
### 1st laplacian eigenmap
#### group-level
![](figs/laplacian.1.png)
#### 100206
![](figs/100206/laplacian.1.png)
#### 100307
![](figs/100307/laplacian.1.png)

### 2nd laplacian eigenmap
#### group-level
![](figs/laplacian.2.png)
#### 100206
![](figs/100206/laplacian.2.png)
#### 100307
![](figs/100307/laplacian.2.png)

### 3rd laplacian eigenmap
#### group-level
![](figs/laplacian.3.png)
#### 100206
![](figs/100206/laplacian.3.png)
#### 100307
![](figs/100307/laplacian.3.png) 

## Individual task contrast map reconstruction using individual resting-state SVD (blue) and groupPCA resting-state SVD (orange)
 * x-axis is the number of components used in the reconstruction, and y-axis is the correlation between reconstructed task map and true task map in the left-out region
 * In general, task reconstruction based on group-level resting state is better...
 * Task reconstructions in Dorsal attention, Frontal Parietal networks are better than those in limbic and visual networks
### Subject 100206
#### Task map reconstruction in Dorsal Attention network (with the specific task specified in the title)
![](figs/DorAtt100206.png)

#### Task map reconstruction in Fronto Parietal network (with the specific task name specified in the title)
![](figs/FP100206.png)

#### Task map reconstruction in Limbic network
![](figs/Limbic100206.png)

#### Task map reconstruction in Visual network
![](figs/Visual100206.png)

### subject 100307
#### Task map reconstruction in Dorsal Attention network (with the task name specified in the title)
![](figs/DorAtt100307.png)

#### Task map reconstruction in Fronto Parietal network (with the task name specified in the title)
![](figs/FP100307.png)

#### Task map reconstruction in Limbic network
![](figs/Limbic100307.png)

#### Task map reconstruction in Visual network
![](figs/Visual100307.png)

## Individual task contrast map reconstruction using individual task contrast maps with the reconstructed task category left out
 * x-axis is the number of components used in the reconstruction, and y-axis is the correlation between reconstructed task map and true task map in the left-out region
 * Each line represents the reconstruction in a specific network (Visual, Somatomotor, DorsalAttention, VentralAttension, Limbic, FrontalParietal, Default), and each panel is the reconstruction of a specific task
 * In general, task reconstructions of working memory/relational is better than those of emotion/gambling in Dorsal Attention/Ventral Attention/Frontal Parietal networks...
### Subject 100206
![](figs/task_100206.png)

### Subject 100307
![](figs/task_100307.png)
