## Flip the matrix - IDPs as samples and subjects as features
### UKB
[Laplacian Eigenmap (first two components) as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.2.LE.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.5.min_samples.5.0.metric.SIMLR.simlr_components.10.colors.CombinedModalityTypes.html)
![](static_figures/ALL_deconfnoage.2.LE.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.5.min_samples.5.0.metric.SIMLR.simlr_components.10.colors.CombinedModalityTypes.png)

[CombinedModalityTypes itself as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.2.CombinedModalityTypes.nr_cubes.15.overlap.0.2.DBSCAN.eps.2.5.min_samples.10.0.metric.SIMLR.simlr_components.10.colors.CombinedModalityTypes.html)

### HCP
[Laplacian Eigenmap (first two components) as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/hcp_idps_all_deconf.2.LE.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.5.min_samples.10.0.metric.SIMLR.simlr_components.10.colors.modality.html)

[Modality itself as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/hcp_idps_all_deconf.2.modality.nr_cubes.15.overlap.0.3.DBSCAN.eps.2.5.min_samples.5.0.metric.SIMLR.simlr_components.10.colors.modality.html)
## delta2q_sex as filter, age/delta2q/sex as color
### sex as color
[sex - neighbourhood radius 1.0](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.sex.html)
![](static_figures/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.sex.png)

### age as color
[age - neighbourhood radius 1.0](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.html)
![](static_figures/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.png)

### delta2q as color
[delta2q - neighbourhood radius 1.0](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.html)
![](static_figures/ALL_deconfnoage.1.delta2q_sex.nr_cubes.20.overlap.0.4.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.png)

## delta2q as filter, age/delta2q/sex as color
### age as color
[age - neighbourhood radius 1.0](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.html)
![](static_figures/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.0.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.png)

[age - neighbourhood radius 1.1](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.1.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.html)
![](static_figures/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.1.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.png)

[age - neighbourhood radius 1.2](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.2.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.html)
![](static_figures/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.2.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.age.png)

### delta2q as color
[delta2q - neighbourhood radius 1.1](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.1.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.html)
![](static_figures/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.1.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.png)

[delta2q - neighbourhood radius 1.2](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.2.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.html)
![](static_figures/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.2.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.png)

### sex as color
[sex - neighbourhood radius 1.1](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.1.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.sex.html)

[sex - neighbourhood radius 1.2](https://yingqiuzheng.me/meeting_archive/2019_oct_24/htmls/ALL_deconfnoage.1.delta2q.nr_cubes.15.overlap.0.3.DBSCAN.eps.1.2.min_samples.20.0.metric.SIMLR.simlr_components.10.colors.delta2q.html)

## To Do List
* DBSCAN seems to be a bad choice of clustering (as non-core points outside the neighbourhood radius of core points were classified as noise and abondoned afterwards); One alternative is single-linkage clustering. Under the premise that within-cluster distances are smaller than between-cluster distances, the distance threshold to cut off the clustering can be set to the first empty bin of distance histogram for each mapper cube;

* Find a better application than simply trying different combinations on the N \times p point cloud, for example, construct Hausdorff distance matrix as feature to conduct manifold comparison

* Apply TDA to time-evolving signals, e.g., resting-state fMRI (time points as samples, voxels as features) to find distinct modes across time
