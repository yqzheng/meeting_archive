### 0) user all 100 maps (ICA maps for bases) for dualregression.  But then when you use the bases to learn the prediction, just use the 55 good maps.
 * Surprisingly, using good components only in shape prediction decrease model accuracy... - perhaps regress out the bad (DR) components fromcontrast maps?
 * Yellow bar: prediction using all 100 maps; blue bar: predictions using 55 good maps. Dual regression was conducted using all 100 maps.
 * Left panel: prediction using original dual regression maps; rght panel: prediction using residual maps (regressed out group level effects)
![](figs/all_vs_good_components.png)

 * model accuracy across 91 subjects (shadows being 95% bootraping CI)
 * Blue curve: prediction using all components
 * orange curve: prediction using good components
###### prediction using original basis/contrasts (individual level)
![](figs/curves.png)
###### prediction using residual basis/contrasts (individual level)
![](figs/curves_reg.png)

### 1) use unthresholded group maps when regressing group mean maps out of subject bases and taskmaps**
Done. Difference is trivial (not shown).

### 1b) show histograms/boxplots of betas (one histogram per task contrast)
#### UKB data (91 subjects)
##### betas of the prediction using all 100 components, with the good components specified in the xasis
![](figs/betas_all.png)
##### betas of the prediction using 55 good components only
![](figs/betas_good.png)

#### HCP data (98 subjects, same as the ones used in the Science paper)
##### betas of prediction using 50 DR maps (as ICA50 is better than ICA100, see the next section)
 * Emotions
![](figs/betas_emotion.png)
 * Relational
![](figs/betas_relational.png)
 * Motor
![](figs/betas_motor.png)

### 1c) SEPARAETLY from shape prediction, can we predict overall task amplitude
 * Regressors are std of time courses in dual regression concatenated across subjects (subjectsXcomponents)
 * Responses are amplitudes concatenated across subjects (subjectsXtasks)
 * Amplitude prediction was trained in 80% subjects and predict the other 20%, repeat 100 times
 
#### HCP data amplitude prediction (971 subjects, ICA50 DR maps)
![](figs/amplitude_prediction_hcp.png) 

#### UKB data amplitude prediction (600 subjects, ICA100 DR maps - all components)
<img src="figs/amplitude_prediction_ukb.png" width="50%">
 
### 2) use more spherical dilation for masking predictions for accuracy evaluation
 * after applying a mask based on group-level contrast maps improved self vs other increase (difference between the mean of diagonal and off diagonal elements)
![](figs/dilate.png)
 * after applying a mask suppress off-diagnoal "model accuracy" but diagnoal remains relatively unchanged 
![](figs/corr_mat_difference.png)
 * normalise columns and rows
![](figs/corr_mat_difference_reg.png)
 * using residual maps to do shape prediction
![](figs/dilate_reg.png)

### 3) compare against Saad's "Ido" results
 * same subset of subjects were used
 * More increase in self vs other (difference between the means of diagonal elements and off-diagonal elements, relative to the mean of off-diagonal elements)
 * Actual values of model accuracy is not as good 
![](figs/improv_both.png)
![](figs/model_accuracy_both.png)

### 4) revisit question of different basis types (ICA, eigenmaps etc) and different dimensionalities.
 * blue curve: laplacian eigenmaps; orange curve: pca; green curve: ica
 * At each dimensionality, pca components and ica components span the same subspace
 * ica maps are marginally better than the other two
![](figs/basis_comparison.png)
![](figs/basis_comparison_reg.png)

### 5)  instead of using a left-out subject's bases and the pretrained (averaged) betas, you could match the left-out subject's bases to the training bases - eg find the 100 closest subjects - and use the bases and betas from those 100 subjects to do the prediction.
 * yellow box: prediction using the self basis and others coefficients (leave-one-out)
 * blue box: prediction using 100 others basis (which matches the self subject best) and coefficients, weighted averaged according to the correlation of the first 30 maps with the self subject (best 100)
##### HCP data
![](figs/leave_one_out_vs_best100.png)
![](figs/leave_one_out_vs_best100_reg.png)

