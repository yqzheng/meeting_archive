## UKB pairwise data - (relatively) low correlation and high mutual information
Calculate pairwise mutual information, and choose IDP pairs that have relatively low correlation and high mutual information
#### *aseg_lh_intensity_vessel* (column 2691) and *aseg_lh_volume_vessel* (column 2707)
* filtered on *aseg_lh_intensity_vessel*, coloring on *aseg_lh_volume_vessel* (middle) *aseg_lh_intensity_vessel* (right)
* correlation 0.3419 (upper 1% percentile) mutual information (upper 0.06% percentile)
<img src="figs/vessel_indensity_volume.png" width="32%">
<img src="figs/2691-2707.2691.png" width="32%">
<img src="figs/2691-2707.2707.png" width="32%">

#### *IDP_T1_FAST_ROIs_R_cerebellum_X* (column 182) and *IDP_SWI_T2star_right_putamen* (189)
* filtered on *IDP_T1_FAST_ROIs_R_cerebellum_X*, coloring on *IDP_T1_FAST_ROIs_R_cerebellum_X* (middle) and *IDP_SWI_T2star_right_putamen* (right)
* correlation -0.0438 (upper 20% percentile) mutual information 0.4020 (upper ~0.3% percentile)
<img src="figs/cerebellum_putamen.png" width="32%">
<img src="figs/182-189.182.png" width="32%">
<img src="figs/182-189.189.png" width="32%">

## HCP individual task data (working memory)
* 405 timeframes by 91282 voxels for each individual
* each block 25s + 2.5s cue, fixation 15s; 8 blocks in total
* see html files

## HCP resting state temporal pattern
<img src="figs/hcp_temporal.png" width="60%">

## Calculating homology features on surface data (HCP)
* Construct a filtration of simplicial complexes on top of vertices of surface using their actual euclidean distance, and find the lifespan of H0, H1, H2 features
<img src="../2019_nov_11/figs/100307.32k.knn.60.L.persistent_scatter_plot.png" height="100%" width="32%">
<img src="../2019_nov_11/figs/100307.32k.knn.100.L.persistent_scatter_plot.png" height="100%" width="32%">
<img src="../2019_nov_11/figs/100307.32k.knn.200.L.persistent_scatter_plot.png" height="100%" width="32%">

Each point is a birth-death pair. The points that lie on the horizontal infnity line (i.e. the features that survived the whole filtartion) were taken as meaningful features. Counts of these features in each homology group (here we only consider H1 and H2, as there is only one sustained connected component- the whole surface) were correlated with behavioural metrics across subjects (uncorrected):
* Fluid Intelligence (Penn Progressive Matrices): H1 left (r=0.1004, p=0.0015); H1 left (r=0.1172, p=0.0002)
* Self-regulation/Impulsivity (Delay Discounting: Area Under the Curve for Discounting of $200): H1 left (r=0.1441, p=4.60e-6); H1 right (r=0.1618, p=2.60e-7)
* Self-regulation/Impulsivity (Delay Discounting: Area Under the Curve for Discounting of $40,000): H1 left (r=0.1048, p=8.84e-4); H1 right (r=0.1172, p=1.98e-4)
* Emotion Recognition (Penn Emotion Recognition Test): H1 left (r=-0.1002, p=0.0015)
* Language/Vocabulary Comprehension (Picture Vocabulary), ageadj : H1 left (r=0.0794, p=0.0119); H1 right (r=0.0838, p=0.0079)
* Language/Vocabulary Comprehension (Picture Vocabulary), unadj: H1 left (r=0.0919, p=0.0036); H1 right (r=0.0909, p=0.0039)
* Episodic Memory (Picture Sequence Memory) ageadj: H2 right (r=0.0975, p=0.002)
* Episodic Memory (Picture Sequence Memory) unadj: H2 right (r=0.0963, p=0.0023)
