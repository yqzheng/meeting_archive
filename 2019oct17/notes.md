## Age as a 1-D filter
### Effects of overlapping percentage of the bins (*gains* in the language of TDA)
Overlapping percentage may change the assignment of data points to the bins thus affect clustering within each bin. I found, generally, greater overlapping of the bins tend to increase ''clustering coefficient'', i.e., two nodes that share a common neighbour are more likely to be connected.
* overlap_perc 0.5 vs overlap_perc 0.6 (other parameters fixed)

![20-0.5-0.8](age_as_filter/overlap.0.5.eps.0.8.nrcubes.20.png)
[20 bins 0.5 overlapping 0.8 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.0.8.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.6-0.8](age_as_filter/overlap.0.6.eps.0.8.nrcubes.20.png)
[20 bins 0.6 overlapping 0.8 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.6.DBSCAN.eps.0.8.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.5-1.0](age_as_filter/overlap.0.5.eps.1.0.nrcubes.20.png)
[20 bins 0.5 overlapping 1.0 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.1.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.6-1.0](age_as_filter/overlap.0.6.eps.1.0.nrcubes.20.png)
[20 bins 0.6 overlapping 1.0 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.6.DBSCAN.eps.1.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.5-1.2](age_as_filter/overlap.0.5.eps.1.2.nrcubes.20.png)
[20 bins 0.5 overlapping 1.2 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.1.2.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.6-1.2](age_as_filter/overlap.0.6.eps.1.2.nrcubes.20.png)
[20 bins 0.6 overlapping 1.2 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.6.DBSCAN.eps.1.2.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

## Effects of clusterer parameters (Density-based spatial clustering)
Epsilon is the largest distance between points A and B to consider A and B are neighbours. Points are more likely to merge together under larger epsilon (thus different branches may merge into a single one). 
* epsilon 0.8, 1.0, 1.2 (based-on histogram of similarity metrics) under 15 bins and 0.5 overlap

![15-0.5-0.8](age_as_filter/overlap.0.5.eps.0.8.nrcubes.15.png)
[15 bins 0.5 overlapping 0.8 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.0.8.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![15-0.5-1.0](age_as_filter/overlap.0.5.eps.1.0.nrcubes.15.png)
[15 bins 0.5 overlapping 1.0 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.1.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![15-0.5-1.2](age_as_filter/overlap.0.5.eps.1.2.nrcubes.15.png)
[15 bins 0.5 overlapping 1.2 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.1.2.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

## Effects of number of bins, 15 bins vs 20 bins (1-D filter space)
Number of bins does not substantially affect the layout of the graph except slightly increasing the number of nodes and revealing more details...
![15-0.5-0.8](age_as_filter/overlap.0.5.eps.0.8.nrcubes.15.png)
[15 bins 0.5 overlapping 0.8 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.0.8.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.5-0.8](age_as_filter/overlap.0.5.eps.0.8.nrcubes.20.png)
[20 bins 0.5 overlapping 0.8 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.0.8.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![15-0.5-1.0](age_as_filter/overlap.0.5.eps.1.0.nrcubes.15.png)
[15 bins 0.5 overlapping 1.0 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.1.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.5-1.0](age_as_filter/overlap.0.5.eps.1.0.nrcubes.20.png)
[20 bins 0.5 overlapping 1.0 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.1.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![15-0.5-1.2](age_as_filter/overlap.0.5.eps.1.2.nrcubes.15.png)
[15 bins 0.5 overlapping 1.2 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.15.overlap.0.5.DBSCAN.eps.1.2.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

![20-0.5-1.2](age_as_filter/overlap.0.5.eps.1.2.nrcubes.20.png)
[20 bins 0.5 overlapping 1.2 epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/age_as_filter/ALL_noage.1.age.nr_cubes.20.overlap.0.5.DBSCAN.eps.1.2.min_samples.1.metric.SIMLR.simlr_components.10.colors.age.noage.html)

## PCA components as filters
The first and third components correlate most with age (|r|=0.5186 for the first and |r|=0.2831 for the third).

[first-component as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_17/PCA_as_filter/ALL_noage.1.PCA.nr_cubes.15.overlap.0.8.DBSCAN.eps.1.5.min_samples.20.metric.SIMLR.simlr_components.10.colors.age.noage.html)

[first-third-components as filter](https://yingqiuzheng.me/meeting_archive/2019_oct_17/PCA_as_filter/ALL_noage.1.PCA.nr_cubes.15.overlap.0.5.DBSCAN.eps.1.5.min_samples.20.metric.SIMLR.simlr_components.10.colors.age.noage.html)

[first-third-components as filter, slightly increase epsilon](https://yingqiuzheng.me/meeting_archive/2019_oct_17/PCA_as_filter/ALL_noage.1.PCA.nr_cubes.15.overlap.0.5.DBSCAN.eps.2.0.min_samples.20.metric.SIMLR.simlr_components.10.colors.age.noage.html)  

## Discussion
### Mapper used on neuroimaging point clouds is very sensitive to the choice of filter functions. Although Mapper claims to be capalable of visualising the shape of data in the original dimensions, using it with different filter functions often outputs graph objects of different shape. Without ground truth in mind, which ''shape object'' truly reflects the data?
* My understanding is that Mapper was build on topological space (which is usually associated with continuous space), but data represented as discrete and finite observations does not contain topological information *per se*. The sensitivity of the shape object to the choice of filters thus arises from the clustering stage which seeks to build up the nerve to approximate the shape.

### Isn't it circular to use age-related filters to find patterns in age?


