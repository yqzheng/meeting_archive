<style TYPE="text/css">
code.has-jax {font: inherit; font-size: 100%; background: inherit; border: inherit;}
</style>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'] // removed 'code' entry
    }
});
MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML-full"></script>
### Reconstruction of individual task map (with group contrast map NOT regressed out)
#### Reconstruction correlation coefficients (with actual maps) across 89 subjects
 * Group-level Laplacian Eigenmaps, PCA and ICA spatial maps 
 (estimated from MIGP) 
 were regressed into individual time series to get individual time courses. 
 These time courses were subsequently regressed into the original 
 individual time series to get the group-level spatial maps for 
 each individual
 
 * For each subject, reconstruction coefficients were estimated by 
 regressing the dual regression maps into individual task contrast maps
 
 * The products of dual regression maps and reconstruction coefficients 
 (averaged across the unseen 88 subjects) were then compared with the actual 
 task map for the subject.
 
 * Blue curve (Laplacian eigenmaps); red curve (PCA); green curve (ICA). Averaged 
 across subjects with 95% CI 
 
![](figs/curves.png)

#### Correlation matrices of 89 LE-reconstructed maps by 89 actual maps, for each task

![](figs/corr_mat.png)

#### Correlation matrices of 89 LE-reconstructed maps by 89 actual maps column-normalised and then row-normalised

![](figs/corr_mat_z.png)

#### Correlation matrices of 89 PCA-reconstructed maps by 89 actual maps

![](figs/pca.corr_mat.png)

#### Correlation matrices of 89 PCA-reconstructed maps by 89 actual maps, column-normalised and then row-normalised 

![](figs/pca.corr_mat_z.png)

#### Correlation matrices of 89 ICA-reconstructed maps by 89 actual maps

![](figs/fastica.corr_mat.png)

#### Correlation matrices of 89 ICA-reconstructed maps by 89 actual maps, column-normalised and then row-normalised 

![](figs/fastica.corr_mat_z.png)

### Reconstruction of individual task map (with group contrast map regressed out)
#### Reconstruction correlation coefficients (with actual maps) across 89 subjects

![](figs/curves_reg.png)

#### Correlation matrices of 89 LE-reconstructed maps by 89 actual maps, for each task

![](figs/corr_mat_reg.png)

#### Correlation matrices of 89 LE-reconstructed maps by 89 actual maps column-normalised and then row-normalised

![](figs/corr_mat_reg_z.png)

#### Correlation matrices of 89 PCA-reconstructed maps by 89 actual maps

![](figs/pca.corr_mat_reg.png)

#### Correlation matrices of 89 PCA-reconstructed maps by 89 actual maps, column-normalised and then row-normalised 

![](figs/pca.corr_mat_reg_z.png)

#### Correlation matrices of 89 ICA-reconstructed maps by 89 actual maps

![](figs/fastica.corr_mat_reg.png)

#### Correlation matrices of 89 ICA-reconstructed maps by 89 actual maps, column-normalised and then row-normalised 

![](figs/fastica.corr_mat_reg_z.png)

### Potential directions
#### Cross-species analysis 
 * are the bases comparable across mouse/macaque/humans? 
 * if yes, what approaches can be used to compare them? 
 * Bases at voxel-level (without *a priori* parcellations) can be more easily translated to other species...
#### Persisten homology on bases (Lower-star Filtrations)
<img src="figs/lower-star.png" width="33%">

Given a simplicial complex 𝐾 and a real-valued function defined on its vertices, 𝑓: Vrt(𝐾) → ℝ. The star of a vertex 𝑣 *Star(𝑣)* is the set of all simplices in 𝐾 that contain 𝑣 as a vertex; the lower star of 𝑣 is {*w* ∈ *Star(𝑣)* |𝑓(*w*) ≤ 𝑓(𝑣)}. The lower-star filtration of 𝐾 is 𝐾a={𝜎 ∈ 𝐾∣ 𝑓(𝑣) ≤ 𝑎 for all 𝑣∈𝜎}
