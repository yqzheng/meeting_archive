## Two synthetic studies
### Study I 
Suppose three IDPs, *IDP_T1_FAST_ROIs_L_thalamus* (X axis), *IDP_T1_FAST_ROIs_L_hippocampus* (Y axis), and *IDP_T1_FAST_ROIs_L_amygdala* (Z axis), jointly determine subtypes of a disease. Subjects were selected to make the layout of the subselected point cloud in the aforementioned 3-D space extend in three directions, each one denoting a distinct "subtype" (Fig 1A). Mapper was conducted on the whole IDPs matrix (21407 \times 3913) with age regressed out. Filters were chosen to be X-Y, X-Z, and X (Fig 1B, 1C, 1D respectively). Relatively robust to the choice of bins and clustering parameters, choosing X-Y and X-Z filters can recover the shape of the data while X filter cannot (NB clustering was conducted on the whole IDP set).  
![Fig1](figures/1x/test1.png)

### Study II
Suppose two IDPs, *IDP_T1_FAST_ROIs_L_thalamus* (X axis) and *IDP_T1_FAST_ROIs_L_hippocampus* (Y axis), jointly determine subtypes of a disease (each half denotes a distinct "subtype"). Mapper was conducted on the whole IDPs matrix (21407 \times 3913) with age regressed out.  Filters were chosen to be X-Y, X-Z, and X (Fig 1B, 1C, 1D respectively). Choosing X-Y as filters can recover the shape of the data while X or X-Z filters cannot (NB clustering was conducted on the whole IDP set).
![Fig2](figures/1x/test2.png)
However clustering on a smaller matrix (three dimensions only, X-Y-Z), X or X-Z as filters successfully recover the shape:
<img src="figures/1x/test2_smaller.png" alt="Fig3" width="500"/>

Finding patterns is highly dependent on the choice of filters. It seems crucial to choose filters of direct relevance and remove noise features in the data matrix.

## Brain delta and aging
An interesting question to ask would be if there is different aging patterns or aging modes in the UKB population, e.g. is there distinct modes/patterns of delta2q with age as filter (so that binning takes place on age) or vice versa?
* delta2q (together with sex) as filter and age as measure of interest, Mini-batch K-means clustering - aging are almost homogeneous across delta2q bins...
![](figures/1x/delta2q_sex_as_filters.png)
* DBSCAN clustering (which primarily requires two parameters, neighbourhood radius *epsilon* and minimal samples within a neighbourhood *min_samples*; datapoints with more than *min_samples* neighbours within *eps* are considered as core-points; non-core datapoints outside *eps* of the core points are classified as noise and abondoned).
⋅⋅* with a relatively small *eps* - Almost half of the data points were classified as noise, while on the remaining points age and delta21 have opposition transition patterns
![](figures/1x/delta2q_sex_as_filters_DBSCAN_small_eps.png)
⋅⋅* with a larger *eps* - The points that were classified as noise in the above figure began to appear as core-points so that the graph objects became noisier
![](figures/1x/delta2q_sex_as_filters_DBSCAN.png)
⋅⋅* with a large *eps*i - The points that were classified as noise merged with the core points and the age pattern disappears (while delta2q pattern becomes more evident)
![](figures/1x/delta2q_sex_as_filters_DBSCAN_large_eps.png)
### age (together with sex) as filter and delta2q as measure of interest
Overall it is similar to the case where delta2q was taken as filter and age as measure of interest - when *eps* is small, most points were labelled as noise and discarded while the remaining points revealed that age has an opposite tendency with delta2q; as *eps* increases this relationship disappears. A possible explanation for the age-delta2q pattern when *eps* is small is that there is beyond-quadratic effects of age (so that for larger delta2q, people with relatively small actual age are closer together while the others are scattered around, far apart from each other, and vice versa). Is it possible that there is beyond-quadratic effects of age?

## UKB disease-related 
### Deconfouding seems not perfect within subpopulations especially with respect to a bimodal variable (or binary variable)
 * A mapper study: within each modality, a combination of IDPs that covaries the most with *Non-cancer illness code, self-reported (hypertension)* was selected, resulting in a 21407 by 13 data matrix. Mapper was conducted on this matrix using the same variable *Non-cancer illness code, self-reported (hypertension)* as a 1-D filter. Age and all other confounding factors have been regressed out.
![](figures/1x/hypertension_as_filter.png)
 * hypertension (normalised and deconfounded) has a bimodal distribution and little correlation with sex or age, but in the graph object, different sexes or age population alternate along the hypertension axis...
 * actually within each subpopulation (definded by the two modals of the distribution of hypertension) sex or age still has a significate correlation with the ''deconfounded'' hypertension. It suggests that although deconfouding across the whole population seemingly removes the effects of confounding factors, within subpopulation (e.g., variables that have binary values or bimodal distribution) the confounds are yet to be better accounted for.
 * for example, suppose x=1:100 and y=[50:-1:1, 50:-1:1]. The correlation between x and y is roughly -0.5, and after regressing y out of x, the correlation between x_deconf and y becomes almost zero. However x_deconf(1:50) and y(1:50) are still highly correlated (not a good example though...)


