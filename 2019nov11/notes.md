## HCP and mapper...
### Group results - Learn a low-dimensional group-level embedding by integrating individual information
* Align individual resting state fMRI (4800 timepoints by 15 ICA components) across time, resulting in a 4800 by 15045 components matrix under the assumption that individuals are synchronised
* Use diffusion map to do dimensionality reduction, reducing the matrix to a subspace
* Take the first three DiffuionMap component as filters and do mapper
* 6 DiffusionMap components as input (left) - 20 DiffusionMap components as input (middle) - 100 DiffusionMap components as input (right) - almost identical, which is not beyond expectation because they have the same filters...
<img src="figs/dm6.png" width="32%">
<img src="figs/dm20.png" width="32%">
<img src="figs/dm100.png" width="32%">

future directions
* What are the interpretations of the loops? Do they represent different trajectory or repetative patterns?
* construct probability/entropy of a given state, transition between different states...

### Individual results
* Example: subject 100307, 10 Laplacian Eigenmap components as input, the first two/three components as filters - very noisy
<img src="figs/le2.png" width="32%">
<img src="figs/le3.png" width="32%">

* 10 Diffusion Map components as input, the first three components as filter - nothing...
<img src="figs/indiv_dm.png" width="32%">

## UKB and mapper... nothing interesting (again)!
Possible reasons:
* Data within same bins are almost homogeneous, and clustering cannot distinguish distinct groups (if any) based on simple distance metrics - (optimise clustering within each bin?)
* The variables under investigation are unimodal and do not involve multiple subtypes/patterns...
### Hypertein, age and sex (again...)
Input data matrix: 21407 subjects by 86 IDPs that have |r|>0.1 correlation with hypertension (deconf)
* filters: hypertension and age
<img src="figs/hypertension_age.hypertension.png" width="30%">
<img src="figs/hypertension_age.age.png" width="30%">
<img src="figs/hypertension_age.sex.png" width="30%">

* filters: hypertension and sex
<img src="figs/hypertension_sex.hypertension.png" width="30%">
<img src="figs/hypertension_sex.age.png" width="30%">
<img src="figs/hypertension_sex.sex.png" width="30%">

* filters: first component of PCA on the 86 IDPs and sex
<img src="figs/pca_age.hypertension.png" width="30%">
<img src="figs/pca_age.age.png" width="30%">
<img src="figs/pca_age.sex.png" width="30%">

### delta2q, age and sex (again... no patterns)
We would like to find distinct aging modes...

Input matrix: all subjects by all IDPs, deconfnoage
* age and sex as filters, delta2q as colors (left) and age as colors (right)
<img src="figs/age_sex.delta2q.png" width="30%">
<img src="figs/age_sex.age.png" width="30%">

* delta2q and sex as filters, age as colors (left) and delta2q as colors (right)
<img src="figs/delta2q_sex.age.png" width="30%">
<img src="figs/delta2q_sex.delta2q.png" width="30%">

Input matrix: all subjects by 30 PLS components (linear combinations) that covaries with age
(Almost identical results
* delta2q, sex as filters, age as colors (left) and delta2q as colors (right)
<img src="figs/pls_delta2q_sex.age.png" width="30%">
<img src="figs/pls_delta2q_sex.delta2q.png" width="30%">

* age, sex as filters, age as colors (left) and delta2q as colors (right)
<img src="figs/pls_age_sex.age.png" width="30%">
<img src="figs/pls_age_sex.delta2q.png" width="30%">

### diabetes, age and sex
Input matrix: all subjects by 30 PLS components that covaries with diabetes (deconf)
* diabetes, sex as filters and diabetes as colors (left), age as colors (middle) and sex as colors (right)
<img src="figs/pls_diabetes_sex.diabetes.png" width="30%">
<img src="figs/pls_diabetes_sex.age.png" width="30%">
<img src="figs/pls_diabetes_sex.sex.png" width="30%">

* diabetes, age as filters and diabetes as colors (left), age as colors (middle) and sex as colors (right)
<img src="figs/pls_diabetes_age.diabetes.png" width="30%">
<img src="figs/pls_diabetes_age.age.png" width="30%">
<img src="figs/pls_diabetes_age.sex.png" width="30%">

## Calculating homology features on surface data (HCP)
* Construct a filtration of simplicial complexes on top of vertices of surface using their actual euclidean distance, and find the lifespan of H0, H1, H2 features
<img src="figs/100307.32k.knn.60.L.persistent_scatter_plot.png" height="100%" width="32%">
<img src="figs/100307.32k.knn.100.L.persistent_scatter_plot.png" height="100%" width="32%">
<img src="figs/100307.32k.knn.200.L.persistent_scatter_plot.png" height="100%" width="32%">

Each point is a birth-death pair. The points that lie on the horizontal infnity line (i.e. the features that survived the whole filtartion) were taken as meaningful features. Counts of these features in each homology group (here we only consider H1 and H2, as there is only one sustained connected component- the whole surface) were correlated with behavioural metrics across subjects (uncorrected):
* Fluid Intelligence (Penn Progressive Matrices): H1 left (r=0.1004, p=0.0015); H1 left (r=0.1172, p=0.0002)
* Self-regulation/Impulsivity (Delay Discounting: Area Under the Curve for Discounting of $200): H1 left (r=0.1441, p=4.60e-6); H1 right (r=0.1618, p=2.60e-7)
* Self-regulation/Impulsivity (Delay Discounting: Area Under the Curve for Discounting of $40,000): H1 left (r=0.1048, p=8.84e-4); H1 right (r=0.1172, p=1.98e-4)
* Emotion Recognition (Penn Emotion Recognition Test): H1 left (r=-0.1002, p=0.0015)
* Language/Vocabulary Comprehension (Picture Vocabulary), ageadj : H1 left (r=0.0794, p=0.0119); H1 right (r=0.0838, p=0.0079)
* Language/Vocabulary Comprehension (Picture Vocabulary), unadj: H1 left (r=0.0919, p=0.0036); H1 right (r=0.0909, p=0.0039)
* Episodic Memory (Picture Sequence Memory) ageadj: H2 right (r=0.0975, p=0.002)
* Episodic Memory (Picture Sequence Memory) unadj: H2 right (r=0.0963, p=0.0023)

