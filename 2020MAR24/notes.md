### betas of components in residual prediction, plotted across representative contrasts in one figure
 * HCP
![](figs/betas_all.png)
 * UKB
![](figs/betas_all_ukb.png)

### Amplitude prediction
(purely a typo last time.. )
 
 * HCP amplitude prediction 
![](figs/amplitude_prediction_hcp.png)
 * UKB amplitude prediction
<img src="figs/amplitude_prediction_ukb.png" width="50%">

### What does residual prediction give us? add group effects back on
 * cortical vertices only
![](figs/model_accuracy_ido.png)
![](figs/model_accuracy_cortical.png)
 * cortical vertices + subcortical volume 
![](figs/model_accuracy.png)

### prediction using betas from 100 subjects that best match the subject
 * HCP data (residual regression) ... 
![](figs/leave_one_out_vs_best100_reg.png)
<img src="figs/leave_one_out_vs_best100_reg_ukb.png" width="50%"> 
