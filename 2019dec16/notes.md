## Individual laplacian eigenmaps vs group laplacian eigenmaps
### project task contrast maps (individual level) to the group laplacian eigenmaps (red) and the individual laplacian eigenmaps (blue) and compare the distributions of the two for each subject (with the largest value normalised to 1) 
In general, the distribution of individual laplacian eigenmap projections is "fatter" than that of group laplacian projections...
#### subject 1
<img src="figs/subject.1.spectral.tfmri_emotion_faces.png" width="40%">
<img src="figs/subject.1.spectral.tfmri_gambling_punish.png" width="40%">
<img src="figs/subject.1.spectral.tfmri_motor_lf.png" width="40%">
<img src="figs/subject.1.spectral.tfmri_wm_0bk.png" width="40%">
<img src="figs/subject.1.spectral.tfmri_social_random_tom.png" width="40%">
<img src="figs/subject.1.spectral.tfmri_social_random.png" width="40%">

#### subject 2
<img src="figs/subject.2.spectral.tfmri_emotion_faces.png" width="40%">
<img src="figs/subject.2.spectral.tfmri_gambling_punish.png" width="40%">
<img src="figs/subject.2.spectral.tfmri_motor_lf.png" width="40%">
<img src="figs/subject.2.spectral.tfmri_wm_0bk.png" width="40%">
<img src="figs/subject.2.spectral.tfmri_social_random_tom.png" width="40%">
<img src="figs/subject.2.spectral.tfmri_social_random.png" width="40%">

### compare the distribution of the projections across subjects for each laplacian eigenmap
For most eigenmaps, individual eigenmap projections are more right skewed than group eigenmap projections....

#### tfmri_gambling_punish, the distribution of first four projections across subjects
<img src="figs/laplacian_eigenmap.0.tfmri_gambling_punish.png" width="40%">
<img src="figs/laplacian_eigenmap.1.tfmri_gambling_punish.png" width="40%">
<img src="figs/laplacian_eigenmap.2.tfmri_gambling_punish.png" width="40%">
<img src="figs/laplacian_eigenmap.3.tfmri_gambling_punish.png" width="40%">
<img src="figs/laplacian_eigenmap.4.tfmri_gambling_punish.png" width="40%">
<img src="figs/laplacian_eigenmap.5.tfmri_gambling_punish.png" width="40%">

#### tfmri_emotion_faces, the distribution of first four projections across subjects
<img src="figs/laplacian_eigenmap.0.tfmri_emotion_faces.png" width="40%">
<img src="figs/laplacian_eigenmap.1.tfmri_emotion_faces.png" width="40%">
<img src="figs/laplacian_eigenmap.2.tfmri_emotion_faces.png" width="40%">
<img src="figs/laplacian_eigenmap.3.tfmri_emotion_faces.png" width="40%">
<img src="figs/laplacian_eigenmap.4.tfmri_emotion_faces.png" width="40%">
<img src="figs/laplacian_eigenmap.5.tfmri_emotion_faces.png" width="40%">

#### task contrast map reconstruction at individual level
<img src="figs/correlation.png">
