## Summary
### 1) What is the set of bases that can better represent task-activation in individuals? - how to find a better set of bases? - more advanced methods may come in...(factorise resting data into modes..)
* for now we focus on the comparison of simple methods, i.e., ICA, PCA, Laplacian eigenmaps (with number of components predetermined)
 * 100 components
![](figs/basis_comparison_boxplot.png)
 * 50 components
![](figs/basis_comparison_boxplot50.png)
* automatically find the number of components? Bayesian methods? 
* For now the basis only used information from resting state data of the subject and other subjects, maybe try to incorprate information from other subjects' task activation information?

### 2) using residual basis of the self subject and the coefficients of other subjects to make predictions
##### Using residual bases to make prediction differentiates self from others more than using original basis
 * left panel: prediction using original basis; right panel: prediction using residual basis

<img src="figs/improv.png" width="49%"> <img src="figs/improv_reg.png" width="49%"> 
 * but if add group effects back on:

<img src="figs/improv.png" width="49%"> <img src="figs/improv_reg_addback.png" width="49%">
 * so it is probably biased by group level effects...
##### using residual bases to make prediction also improves prediction accuracy
![](../2020MAR24/figs/model_accuracy.png)
 * How to further improve the accuracy? - improve bases and coefficients used in the prediction
 * Use linear combination of other subjects' coefficients (e.g. weighted average) to do the prediction - seems improvement is very trivial
![](figs/all_vs_best100_reg.png)
 * Or learn a new transformation (under some contraints) to apply to the coefficients or bases
 * learn the relatedness of different tasks so that shared information could be used (coefficients are low-rank?) This might improve accuracy in motor domain
 * Apply a prior to the coefficients (or the bases)... and incorporate a Bayesian approach

### 3) Amplitude prediction
![](../2020MAR24/figs/amplitude_prediction_hcp.png)

