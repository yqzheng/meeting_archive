% Created by Bonita Graham
% Last update: February 2019 By Kestutis Bendinskas

% Authors: 
% Please do not make changes to the preamble until after the solid line of %s.

\documentclass[10pt]{article}
\usepackage[explicit]{titlesec}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1em}
\usepackage{hyphenat}
\usepackage{hyperref}
\usepackage[round]{natbib}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta, urlcolor=blue,
    citecolor=black
}
\usepackage{ragged2e}
\RaggedRight

%\addbibresource{reference.bib}
% These commands change the font. If you do not have Garamond on your computer, you will need to install it.
\usepackage{garamondx}
\usepackage[T1]{fontenc}
\usepackage{amsmath, amsthm}
\usepackage{graphicx}

% This adjusts the underline to be in keeping with word processors.
\usepackage{soul}
\setul{.6pt}{.4pt}


% The following sets margins to 1 in. on top and bottom and .75 in on left and right, and remove page numbers.
\usepackage{geometry}
\geometry{vmargin={1in,1in}, hmargin={.75in, .75in}}
\usepackage{fancyhdr}
\pagestyle{fancy}
\pagenumbering{gobble}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.0pt}

% These Commands create the label style for tables, figures and equations.
\usepackage[labelfont={footnotesize,bf} , textfont=footnotesize]{caption}
\captionsetup{labelformat=simple, labelsep=period}
\newcommand\num{\addtocounter{equation}{1}\tag{\theequation}}
\renewcommand{\theequation}{\arabic{equation}}
\makeatletter
\renewcommand\tagform@[1]{\maketag@@@ {\ignorespaces {\footnotesize{\textbf{Equation}}} #1.\unskip \@@italiccorr }}
\makeatother
\setlength{\intextsep}{10pt}
\setlength{\abovecaptionskip}{2pt}
\setlength{\belowcaptionskip}{-10pt}

\renewcommand{\textfraction}{0.10}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{0.85}
\renewcommand{\floatpagefraction}{0.90}

% These commands set the paragraph and line spacing
\titleformat{\section}
  {\normalfont}{\thesection}{1em}{\MakeUppercase{\textbf{#1}}}
\titlespacing\section{0pt}{0pt}{-10pt}
\titleformat{\subsection}
  {\normalfont}{\thesubsection}{1em}{\textit{#1}}
\titlespacing\subsection{0pt}{0pt}{-8pt}
\renewcommand{\baselinestretch}{1.15}

% This designs the title display style for the maketitle command
\makeatletter
\newcommand\sixteen{\@setfontsize\sixteen{16pt}{6}}
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
\vspace{-.375in}
\sixteen\bfseries \@title
\medskip
\end{flushleft}
\textit{\@author}
\egroup}
\makeatother

% This styles the bibliography and citations.
%\usepackage[biblabel]{cite}
%\usepackage[sort&compress]{natbib}
%\setlength\bibindent{2em}
%\makeatletter
%\renewcommand\@biblabel[1]{\textbf{#1.}\hfill}
%\makeatother
%\renewcommand{\citenumfont}[1]{\textbf{#1}}
%\bibpunct{}{}{,~}{s}{,}{,}
%\setlength{\bibsep}{0pt plus 0.3ex}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Authors: Add additional packages and new commands here.  
% Limit your use of new commands and special formatting.
% Place your title below. Use Title Capitalization.
\title{Topological Data Analysis}

\pagestyle{empty}
\begin{document}

% Makes the title and author information appear.
%\vspace*{-0.5in}
\maketitle
\vspace{-0.5in}

Topological Data Analysis (TDA) primarily includes two techniques, Mapper and persistent homology (PH). Mapper seeks to fit a simplicial complex (often, a graph object) to data. It is closely related with two familiar problems: manifold learning and clustering. In some sense, it is a generalisation of both problems. The goal of PH is to find higher-order topological features in addition to the 0th order feature (e.g., clusters or connected components). PH comes with a ``persistent"" parameter that tells us which topological features live longer than the others. Therefore PH can be used to extract features to be fed into further analysis (e.g., classification). In this summary, we focus on the applications of Mapper on the typical $n\times p$ high-dimensional dataset.

\section*{Mapper vs Manifold Learning}
Both manifold learning techniques and Mapper can be viewed as ``fitting a space to data''. Manifold learning problems can be summarised as finding the intrinsic dimensionality $d$ of of a $n\times p$ matrix where $p>>n$. Just as manifold learning seeks to fit a subspace to data, Mapper fits a topological space to data often in the form of a simplicial complex (note that, if the complex only includes simplices up to dimension 2, i.e., 2-simplex, it becomes a graph object). 

Unlike manifold learning, Mapper does not find intrinsic dimensions $d$ 
but learns the ``shape'' of the data ($n\times p$) in the original space and mapped the shape to a low-dimension space (often 2-D). Therefore it is fundamentally different from manifold learning in that it preserves the shape characteristics of the data without collapsing it into a subspace, even though the shape object is often visualised in a much-lower dimension (for example, \cite{nicolau2011topology} mapped the layout of breast cancer subtypes in the original space to a 1-D bins). This allows Mapper to untangle twisted structures which are invisible to some manifold learning methods which reduce data to a subspace. Consider a 1-D periodic signal embedded in 3-D space for an example (Figure~\ref{fig:fig1}A): highly twisted in nature, the shape of the signal (a circle) is invisible to the popular manifold learning methods when $d=2$ (Figure~\ref{fig:fig1}B) while the loop can be recovered by Mapper and visualised in 2-D space (Figure~\ref{fig:fig1}C). Click \href{https://yingqiuzheng.me/uploads/private/figure1_mapper_interactive.html}{here} for an interactive visualisation (press 's' to highlight the nodes; 'p' to turn the background to white; same as below).
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{figure1.pdf}
\caption{{\bf 1-D Periodic signal (1000 samples) embedded in 3-D space. }A: A period signal generated by $x_1=30\sin(2\pi t) + 20 + \epsilon, x_2=20\cos(3\pi t) + 50 +\epsilon, x_3=-40\sin(\pi t) + 10 +\epsilon$ where $\epsilon\sim\mathcal{N}(0, 1)$. B: A host of manifold learning techniques failed to preserve the shape of the signal in 2-D space. C: Mapper constructed a graph object to approximate the shape of the signal (a loop) and visualised it in 2-D space.}
\label{fig:fig1}
\end{figure}

Consider another example of period signal (not as twisted, but entangled at some point): the signal almost ``crosses'' in the middle, making it difficult to separate the ``knot'', while Mapper again recovers the shape (click \href{https://yingqiuzheng.me/uploads/private/figure2_mapper_interactive.html}{here} for an interactive visualisation).
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{figure2.pdf}
\caption{{\bf 1-D Periodic signal (1000 samples) embedded in 3-D space. }A: A period signal generated by $x_1=30\sin(2\pi t) + 20 + \epsilon, x_2=20\sin(3\pi t) + 50 +\epsilon, x_3=-40\sin(\pi (t+0.05)) + 10 +\epsilon$ where $\epsilon\sim\mathcal{N}(0, 1)$. B: A host of manifold learning techniques failed to preserve the shape of the signal in 2-D space. C: Mapper constructed a graph object to approximate the shape of the signal (a loop) and visualised it in 2-D space.}
\label{fig:fig1}
\end{figure}
% Keywords are required.

{\bf A short summary. }Manifold learning outputs data in reduced space $n\times d$, which means it discarded information in the $p-d$ space (although it is often dominated by noise) thus is restricted to the subspace and has limited ability in representing the spatial layout of the original $n\times p$ data (although sometimes it is possible that the shape of data in the subspace sufficiently approximates the shape in the original data, such as Locally Linear Embedding applied to ``swiss rolls''). It is also relatively sensitive to the notion of distance (geometry), making it sometimes incapable of capturing twisted or nearly ``crossing'' structures. On the other hand, Mapper does not output the intrinsic dimension $d$ but can visualise the layout of the original data in an extremely low dimension.

{\bf Disadvantages of Mapper. } Mapper is relatively qualitive in nature compared to manifold learning. It outputs a shape object instead of a $n\times d$ data matrix and inevitably averages samples inside the same node (of the graph object) thus becomes very restrictive to further analysis (for example, it obviously does not allow using the $d$ features to make predictions). Quantitative analysis that can be conducted on Mapper includes charactersing the relationship between node/edge metrics and its values. For example, \cite{saggar2018towards} relates the portion of fMRI categories (demanding tasks or resting-state) within nodes to modularity or core-pheriphery structure; \cite{rizvi2017single} identifies genes with high connectivity/centroid by investigating the layout of the expression values on the graph object. Mapper is helpful only in certain cases, especially when data is twisted in nature or when topolgical properties (e.g., connected components, loops, or cavity) are more important than geometric information.

\section*{Mapper vs clustering} 
Clustering techniques often seek to find discrete clusters (let's focus on traditional clustering  which does not include soft-clustering). These discrete clusters belong to the 0th order homology group (i.e., connected components, from a topological perspective), while Mapper can be extend to the 2th order (or even higher) homology group to reveal more complicated topological patterns in addition to connected components. More importantly, the key insight offered by Mapper is that many ``clusters" in real data are not discrete delineations in the classical sense but a continuum or spectrum (e.g., branches of some single connected component with subgroups tend to be distributed in different ends). This is especially the case in the field of cancer genomics (\cite{lum2013extracting,nicolau2011topology}, Figure~\ref{fig:branches}) and may find its way in subtyping in neuroimaging as well, since dimensions of neurological disorders may be more biologically relevant than subtypes or clusters.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\textwidth]{lum.jpg}
    \includegraphics[width=0.8\textwidth]{nicolau.pdf}
    \caption{Upper panel: figure from \cite{lum2013extracting} showing the layout of patients that are survived or not; lower panel: figure from \cite{nicolau2011topology} showing the subtypes of breast tumors. Both are continuous branches.}
    \label{fig:branches}
\end{figure}

Figure~\ref{fig:scz}A shows an example of Mapper applied to a schizophrenia dataset of 606 subjects and 4371 features (netmets) (\cite{gong2019powerful}). The data was first mapped to a 2-D space (i.e., two lenses, in the language of TDA) generated by Locally Linear Embedding (LLE). The 2-D space was divided into a series of overlapping bins (i.e., covers). Clustering was subsequently conducted on each of the covers in the \textit{original space} using DBSCAN (because it does not require to specify the number of clusters). Edges were added to clusters that share common samples, yielding a graph object which can be visualised in a 2-D plane. The distance metric used in the (partial) clustering stage was adpated from a kernel-based similarity learning methods (\cite{wang2017visualization}). Click \href{https://yingqiuzheng.me/uploads/private/site.-1.direction.1.LocallyLinearEmbedding.nr_cubes.15.overlap.0.4.DBSCAN.eps.4.min_samples.5.metric.SIMLR.html}{here} for an interactive visualisation. To benchmark the result of Mapper against traditional clustering techniques, we included the result of t-SNE conducted on the same similarity matrix (Figure~\ref{fig:scz}B), in which the distributions of the subgroups were not as distinct.

\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{mapper_tsne_scz.pdf}
    \caption{{\bf Mapper applied to sczephrenia dataset. } A: the graph object constructed from the original $606\times4371$ netmats matrix. Patients and normal controls tend to be distributed towards the two ends of the continuum. B: t-SNE was conducted on the same similarity matrix, while the distributions of the two groups were not as evident.}
    \label{fig:scz}
\end{figure}

The most interesting added advantage of Mapper is that it may reveal subtypes invisible to traditional clustering (\cite{lum2013extracting, nicolau2011topology}). The aforementioned studies both showed that the certain subtypes that are concentrated near one end of the graph object were scattered across different clusters in single linkage clustering (Figure~\ref{fig:clustering}). This is probably because traditional clustering, which seeks to break data down to discrete components, may separate data points that are actually close together. On the other hand, although Mapper also involves clustering (on the original data) in the second stage, the procedure is repeated for each overlapping bins and does not consider data points from other bins. Even if neighbouring data points that belong to the same subgroup are not clustered together inside a bin, it is highly probable that the clusters they are classified to share common samples thus are connected by an edge. 

{\bf A short summary. } The added advantage of Mapper against traditional clustering includes (1) Mapper can reveal higher order features in addition to clusters (which belongs to 0th order homology group); (2) in real data ``clusters'' are not necessarily discrete delineations but a continuum so that Mapper may recover the layout of the subtypes without breaking them into discrete clusters; (3) Mapper is more invariant to the notion of distance than traditional clustering (almost) and may find subtypes invisible to the latter.

{\bf Disadvantages of Mapper
(in clustering). } Mapper is very sensitive to the choice of clustering parameters within each bin and also sensitive to the overlapping percentage of the bins (which are called \textit{gains} in the language of TDA) as well as filter functions. It is largely un-supervised and not as easy to be adapted into a supervised method due to its qualitative nature (however, supervised modifications do exist by making data points with same labels concentrated as close as possible within the graph).

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.9\textwidth]{lum_clustering.jpg}
    \includegraphics[width=0.85\textwidth]{nicolau_clustering.jpg}
    \caption{Caption}
    \label{fig:clustering}
\end{figure}
% Start the main part of the manuscript here.
% Comment out section headings if inappropriate to your discipline.
% If you add additional section or subsection headings, use an asterisk * to avoid numbering. 
\newpage
\clearpage

\bibliographystyle{plainnat}
\bibliography{reference}
\end{document}
