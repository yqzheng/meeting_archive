## Spherical harmonics
create sphercial harmonic coefficients based on spatial location (MNI coordinates) of cortical vertices in *S1200.L.sphere.32k_fs_LR.surf.gii* and *S1200.R.sphere.32k_fs_LR.surf.gii*
![](figs/0.0.png)
![](figs/1,0.png)
![](figs/1,-1.png)
![](figs/2,0.png)
![](figs/2,-1.png)
![](figs/2,-2.png)

## Laplacian eigenmaps (eigenvectors of normalised graph laplacian - almost identical to PCA if correlation matrix is used as similarity matrix?)
![](figs/le.1.png)
![](figs/le.2.png)
![](figs/le.3.png)

## Correlate the coefficients with group-level task contrasts
 * Laplacian eigenmap vs spherical harmonics
![](figs/le_sh.png)
 
 * PCA vs spherical harmonics
![](figs/pca_sh.png)

## Graph laplacian transform and spherical harmonics transform for each (group-level) task_contrast
![](figs/spectral.tfmri_wm_2bk_0bk.png)
![](figs/spectral.tfmri_relational_match_rel.png)
![](figs/spectral.tfmri_language_math.png)
![](figs/spectral.tfmri_language_story.png)
![](figs/spectral.tfmri_language_math_story.png)

## Graph laplacian transform on biparte graphs (almost equivalent to CCA?)
![](figs/wm_2bk_0bk_DorAtt_pred.png)
![](figs/wm_2bk_0bk_DorAtt_activation.png)

#### Visual
![](figs/0.png)
#### Somatomotor
![](figs/1.png)
#### Dorsal Attention
![](figs/2.png)
#### Ventral Attension
![](figs/3.png)
#### Limbic
![](figs/4.png)
#### Frontoparietal
![](figs/5.png)
#### Default
![](figs/6.png)
